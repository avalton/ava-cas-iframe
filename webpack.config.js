/* eslint-disable no-var, object-shorthand, no-console, vars-on-top */
const path = require('path');

const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

// const exposeLoader = require('expose-loader');

const buildPath = path.resolve(__dirname, 'dist');

const packageJson = require('./package.json');

const settings = require('./settings.json');

const isProduction = (process.env.NODE_ENV || 'development').toLowerCase() === 'production';

const isDistBuild = (process.env.WEBPACK_DIST_BUILD || 'false').toLowerCase() === 'true';

const entry = {
    avaCas: './app/AvaCAS.js',
};

const plugins = [];

const configForUMD = {
    entry,
    plugins,
    target: 'web',
    devtool: 'source-map',
    output: {
        libraryTarget: 'umd',
        publicPath: '/',
        path: buildPath,
        filename: `[name]${isProduction ? '.min' : ''}.js`,
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    presets: [
                        ['@babel/preset-env', { modules: false }],
                    ],
                },
            },
        ],
    },
};

const configForWindow = {
    mode: 'production',
    entry,
    plugins,
    target: 'web',
    devtool: 'source-map',
    output: {
        library: 'AvaCAS',
        libraryTarget: 'window',
        publicPath: '/',
        path: buildPath,
        filename: `[name].bower${isProduction ? '.min' : ''}.js`,
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    presets: [
                        ['@babel/preset-env', { modules: false }],
                    ],
                },
            },
        ],
    },
};

if (!isDistBuild || !isProduction) {
    plugins.unshift(new CleanWebpackPlugin([buildPath]));
}

if (!isDistBuild) {
    configForUMD.mode = 'development';
    entry.run = './app/run.js';
    plugins.unshift(
        new HtmlWebpackPlugin({
            inject: false,
            title: packageJson.name,
            template: 'app/index.ejs',
            timestamp: (new Date()).toISOString(),
        }));
} else {
    Object.assign(configForUMD, {
        mode: 'production',
        externals: {
            axios: 'axios',
            eventemitter3: 'eventemitter3',
            'pusher-js': 'pusher-js',
            'url-polyfill': 'url-polyfill',
        },
    });
}

module.exports = [configForUMD, configForWindow];
