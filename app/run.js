import AvaCAS from './AvaCAS';

const cas = new AvaCAS(document, SETTINGS);

cas.on(AvaCAS.EVENT_TOKEN, (event) => console.log(event));
cas.on(AvaCAS.EVENT_TOKEN_MISSING, (event) => console.error(event));
cas.on(AvaCAS.EVENT_LOGGED_IN, (event) => console.log(event));
cas.on(AvaCAS.EVENT_LOGGED_OUT, (event) => console.error(event));

cas.init();
