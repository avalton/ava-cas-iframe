const defaultOptions = {
    hideLoader: true,
    icon: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M11 15h2v2h-2v-2zm0-8h2v6h-2V7zm.99-5C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z"/></svg>',
    title: 'Application Suspended!',
    subTitle: 'For further information please contact support.',
    contactInfo: [
        {
            text: 'Iota Networks, LLC.',
            icon: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M11 17h2v-6h-2v6zm1-15C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8zM11 9h2V7h-2v2z"/></svg>',
        },
        {
            text: 'www.cue-Rx.com',
            icon: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M4.5,11 L2.5,11 L2.5,9 L1,9 L1,15 L2.5,15 L2.5,12.5 L4.5,12.5 L4.5,15 L6,15 L6,9 L4.5,9 L4.5,11 Z M7,10.5 L8.5,10.5 L8.5,15 L10,15 L10,10.5 L11.5,10.5 L11.5,9 L7,9 L7,10.5 Z M12.5,10.5 L14,10.5 L14,15 L15.5,15 L15.5,10.5 L17,10.5 L17,9 L12.5,9 L12.5,10.5 Z M21.5,9 L18,9 L18,15 L19.5,15 L19.5,13 L21.5,13 C22.3,13 23,12.3 23,11.5 L23,10.5 C23,9.7 22.3,9 21.5,9 Z M21.5,11.5 L19.5,11.5 L19.5,10.5 L21.5,10.5 L21.5,11.5 Z"/></svg>',
        },
        {
            text: 'technicalsupport@iotacommunications.com',
            icon: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M20,4 L4,4 C2.9,4 2.01,4.9 2.01,6 L2,18 C2,19.1 2.9,20 4,20 L20,20 C21.1,20 22,19.1 22,18 L22,6 C22,4.9 21.1,4 20,4 Z M20,8 L12,13 L4,8 L4,6 L12,11 L20,6 L20,8 Z"/></svg>',
        },
        {
            text: '+1 855 743 6478 (Option 2)',
            icon: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M6.62,10.79 C8.06,13.62 10.38,15.93 13.21,17.38 L15.41,15.18 C15.68,14.91 16.08,14.82 16.43,14.94 C17.55,15.31 18.76,15.51 20,15.51 C20.55,15.51 21,15.96 21,16.51 L21,20 C21,20.55 20.55,21 20,21 C10.61,21 3,13.39 3,4 C3,3.45 3.45,3 4,3 L7.5,3 C8.05,3 8.5,3.45 8.5,4 C8.5,5.25 8.7,6.45 9.07,7.57 C9.18,7.92 9.1,8.31 8.82,8.59 L6.62,10.79 Z"/></svg>',
        },
    ],
    createOnly: false,
};

export default (cas) => (containerElementSelector = '#app', options = defaultOptions) => {
    const parser = new DOMParser();
    const settings = {
        ...defaultOptions,
        ...options,
    };

    const addIcon = (targetElement, icon, size = 24) => {
        const [svg] = parser
            .parseFromString(icon, 'text/html')
            .getElementsByTagName('svg');
        targetElement.appendChild(svg);
        svg.setAttribute('width', size);
        svg.setAttribute('height', size);
        return svg;
    };

    const addText = (targetElement, tagName, text) => {
        const el = document.createElement(tagName);
        el.innerText = text;
        targetElement.appendChild(el);
        return el;
    };

    const suspendElement = document.createElement('div');
    const styles = document.createElement('style');
    const infoBox = document.createElement('div');
    // styles are manually copied from renderAppSuspended.css, prefixed and compressed
    styles.innerHTML = '#app-suspended{z-index:100;position:absolute;top:0;bottom:0;left:0;right:0;background-color:#efefef;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center}#app-suspended *{margin:0;padding:0}#app-suspended .info-box{min-width:320px}#app-suspended .info-box .info-header{text-align:center;fill:#FF3D00;color:#FF3D00}#app-suspended .info-box .info-header svg{margin-bottom:24px}#app-suspended .info-box .info-header h2{margin-bottom:20px;font-size:24px;font-weight:500}#app-suspended .info-box .info-header h6{margin-bottom:24px;font-size:14px;font-weight:400;color:#282828}#app-suspended .info-box .contact-info{padding:8px 16px;background-color:#fff;-webkit-box-shadow:0 0 2px 0 rgba(0,0,0,0.12),0 2px 2px 0 rgba(0,0,0,0.24);box-shadow:0 0 2px 0 rgba(0,0,0,0.12),0 2px 2px 0 rgba(0,0,0,0.24)}#app-suspended .info-box .contact-info .info-row{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:start;-ms-flex-pack:start;justify-content:flex-start;-webkit-box-align:center;-ms-flex-align:center;align-items:center;margin:8px}#app-suspended .info-box .contact-info .info-row svg{display:inline-block;margin-right:16px}#app-suspended .info-box .contact-info .info-row p{display:inline-block;color:#626262;font-size:12px}#app-suspended .info-box .contact-info .info-row:first-child p{font-weight:700}';
    infoBox.setAttribute('class', 'info-box');
    suspendElement.appendChild(styles);
    suspendElement.appendChild(infoBox);
    suspendElement.id = 'app-suspended';
    const containerElement = document.querySelector(containerElementSelector);
    if (settings.hideLoader) {
        cas.hideLoader();
    }
    const infoHeader = document.createElement('div');
    infoHeader.setAttribute('class', 'info-header');
    if (settings.icon) {
        addIcon(infoHeader, settings.icon, 104);
    }
    if (settings.title) {
        addText(infoHeader, 'h2', settings.title);
    }
    if (settings.subTitle) {
        addText(infoHeader, 'h6', settings.subTitle);
    }
    infoBox.appendChild(infoHeader);
    if (settings.contactInfo) {
        const contactInfo = document.createElement('div');
        contactInfo.setAttribute('class', 'contact-info');
        if (!Array.isArray(settings.contactInfo)) {
            console.log('Contact info needs to be an array. Using default `contactInfo`.');
            settings.contactInfo = defaultOptions.contactInfo;
        }
        settings.contactInfo.forEach(({ icon, text }) => {
            const infoRow = document.createElement('div');
            infoRow.setAttribute('class', 'info-row');
            addIcon(infoRow, icon);
            addText(infoRow, 'p', text);
            contactInfo.appendChild(infoRow);
        });
        infoBox.appendChild(contactInfo);
    }
    if (containerElement && !settings.createOnly) {
        containerElement.innerHTML = '';
        containerElement.style.position = 'relative';
        containerElement.appendChild(suspendElement);
    }
    return suspendElement;
};
