import EventEmitter from 'eventemitter3';
import Pusher from 'pusher-js';

class Session extends EventEmitter {
    static get EVENT_LOGIN() { return 'session.login'; }
    static get EVENT_LOGOUT() { return 'session.logout'; }
    constructor(userId = false, token = '', { pusher }) {
        super();
        if (userId) {
          this.socket = new Pusher(pusher.id, {
            cluster: pusher.cluster,
            encrypted: pusher.encrypted,
          });
          this.sessionChannel = this.socket.subscribe(`${userId}-${token}`);
          this.sessionChannel.bind('login', (data) => this.onLogin(data));
          this.sessionChannel.bind('logout', (data) => this.onLogout(data));
        } else {
          console.log('Cannot subscribe to session events without userId');
        }
    }
    onLogin(data) {
        console.log('onLogin', data);
        this.emit(Session.EVENT_LOGIN, data);
    }
    onLogout(data) {
        console.log('onLogout', data);
        this.emit(Session.EVENT_LOGOUT, data);
    }
}

export default Session;
