import renderAppSuspended from './actions/renderAppSuspended';

if (!window._babelPolyfill) {
    require('@babel/polyfill');
}

import { get, find, cloneDeep } from 'lodash-es';
import axios from 'axios';
import EventEmitter from 'eventemitter3';
import { URL } from 'url-polyfill/url';
import Session from './session';

const SYM_TOKEN = Symbol('token');
const SYM_USER_ID = Symbol('userId');

const applicationDefinitions = require('./applications.json');

export const transformUser = (userData, businesses) => {
    const currentUser = cloneDeep(userData);
    try {
        currentUser.guiSettings = JSON.parse(currentUser.guiSettings);
    } catch (error) {
        console.error('Failed to parse user settings, using empty object');
        currentUser.guiSettingsRaw = currentUser.guiSettings;
        currentUser.guiSettings = {};
    }
    currentUser.guiSettings.defaultBusiness = currentUser.guiSettings.defaultBusiness || businesses[0].id;
    currentUser.currentBusiness = cloneDeep(find(businesses, { id: get(currentUser, 'guiSettings.defaultBusiness') }));
    currentUser.previousBusiness = cloneDeep(find(businesses, { id: get(currentUser, 'guiSettings.previousBusiness') }));
    return currentUser;
};

export const transformApplications = (portalLink, applications, urls) =>
    applications.map((app) => {
        const { iconBase64, icon, label, color } = applicationDefinitions[app.name];
        return Object.assign(app, {
            label,
            color,
            link: `https://${urls[app.name].toLowerCase()}`,
            icon: iconBase64 ? `data:image/svg+xml;base64,${iconBase64}` : `${portalLink}/img/appicons/${icon}.svg`,
        });
    });

export const setCurrentApp = (name = '', data = {}) =>
    Object.assign({}, data, { currentApplication: find(data.applications, { name }) || {} });

export const transformToolbarData = ({
    usersUrl,
    currentUser,
    notifications,
    business,
    businesses,
    applications,
    applicationId = '',
    urls,
}) => {
    const portalLink = `${window.location.protocol}//${urls.instance.toLowerCase()}`;
    const data = {
        portalLink,
        currentUser: Object.assign(currentUser, {
            avatar: currentUser.profilePicture ? `${usersUrl}/public/file/${currentUser.profilePicture.id}` : false,
        }),
        notifications,
        business,
        businesses,
        applications: transformApplications(portalLink, applications, urls),
        businessLogo: business.logo ? `${usersUrl}/public/file/${business.logo.id}` : false,
    };
    return setCurrentApp(applicationId, data);
};

class AvaCAS extends EventEmitter {
    static get EVENT_TOKEN() { return 'cas.token'; }
    static get EVENT_USER_ID() { return 'cas.user_id'; }
    static get EVENT_TOKEN_MISSING() { return 'cas.token_missing'; }
    static get EVENT_LOGGED_IN() { return 'cas.logged_in'; }
    static get EVENT_LOGGED_OUT() { return 'cas.logged_out'; }
    static get AVA_TIMEOUT_TIME() { return 10000; }
    constructor(document, settings) {
        console.log('iframe construct', settings);
        super();
        Object.assign(this, {
            document,
            settings,
        });
    }

    get defaultState() {
        return {
            continue: `${window.location.protocol}//${this.urls.instance}`,
        };
    }

    init(forceUser) {
        if (!this.settings || !this.settings.usersUrl) {
            return Promise.reject('AvaCAS - missing usersUrl in settings, won\'t know which server to use for session handling');
        }
        if (this.initPromise) {
            return this.initPromise;
        }
        const { protocol, host } = new URL(this.settings.src || window.location.href);
        this.iframeSrc = `${protocol}//${host}`;
        this.initPromise = new Promise((resolve, reject) => {
            const onMessage = (message) => {
                if (message.origin === this.iframeSrc || message.origin.replace(/\/$/, '') === window.location.origin.replace(/\/$/, '')) {
                    return;
                }
                if (message.origin !== this.settings.usersUrl) {
                    reject(new Error('Unknown iFrame origin'));
                    return;
                }
                this.clearIframeTimeout && this.clearIframeTimeout();
                delete this.clearIframeTimeout;
                const response = {};
                if (typeof message.data === 'string') {
                    try {
                        Object.assign(response, JSON.parse(message.data));
                    }   catch(error) {
                        console.error('Failed to parse message.data', error);
                    }
                }
                if (response.statusCode === 403 && !forceUser) {
                  return onIframeError(new Error(response.message));
                }
                this.urls = response.data.urls;
                if (window.location.host === this.urls.login && ['Safari', 'iPhone', 'iPad'].indexOf(AvaCAS.getBrowserId()) > -1) {
                    const url = new URL(window.location.href);

                    if (url.searchParams.get('safariCookie') !== '1') {
                        url.searchParams.set('safariCookie', 1);
                        window.location = `${this.settings.usersUrl}/v3/users/cookie?redirect=${encodeURIComponent(url.toString())}`;
                        return false;
                    }
                }
                if (forceUser) {
                    this.userId = forceUser.userId;
                    this.token = forceUser.token;
                    response.data.token = forceUser.token;
                }
                else if (response.statusCode === 200) {
                    this.userId = response.data.currentUser.id || this.userId;
                    this.token = response.data.token;
                } else {
                    this.emit(AvaCAS.EVENT_TOKEN_MISSING, { event: AvaCAS.EVENT_TOKEN_MISSING, data: response.data });
                }
                this.data = response.data;
                resolve(response.data);
            };
            const onIframeLoaded = (iframe) => {
                this.initComplete = true;
                this.clearIframeTimeout = ((id) => () => clearTimeout(id))(setTimeout(onIframeError, AvaCAS.AVA_TIMEOUT_TIME));
            };
            const onIframeError = (error) => reject(error || new Error('Failed to load iFrame'));
            const pageLoaded = () => {
                window.addEventListener('message', onMessage);

                //* MAGIC FIX
                if (this.iframeSrc.match(/\/\/tracker/)) {
                    this.settings.appName = 'M2MTracker';
                }
                //* end of MAGIC FIX

                console.log('iframe before', this.settings);
                const noCache = (new Date()).getTime();
                const appName = this.settings.appName ? `&appName=${this.settings.appName}` : '';
                const postMessageSrc = this.settings.postMessageSrc ? `&postMessageSrc=${this.settings.postMessageSrc}` : '';
                this.document.removeEventListener('DOMContentLoaded', pageLoaded);
                const iframe = document.createElement('iframe');
                iframe.setAttribute('width', 0);
                iframe.setAttribute('height', 0);
                Object.assign(iframe.style, {
                  width: '0px',
                  height: '0px',
                  border: 'none',
                  padding: '0px',
                  margin: '0px',
                  display: 'none',
                });
                iframe.onload = () => onIframeLoaded(iframe);
                iframe.src = `${this.settings.usersUrl}/v3/users/status?src=${this.iframeSrc}&no-cache=${noCache}${appName}${postMessageSrc}`;
                this.document.body.appendChild(iframe);
            };
            if (this.document.readyState !== 'loading') {
                pageLoaded();
            } else {
                this.document.addEventListener('DOMContentLoaded', pageLoaded);
            }
        });
        this.initPromise.then(() => this.initResolved(), this.initRejected);
        return this.initPromise;
    }

    initResolved() {
        this.initSession();
    }

    initRejected() {

    }

    initSession() {
        this.session = new Session(this.userId, this.token, this.settings);
        this.session.on(Session.EVENT_LOGIN, (data) => {
            this.emit(AvaCAS.EVENT_LOGGED_IN, { event: AvaCAS.EVENT_LOGGED_IN, data });
        });
        this.session.on(Session.EVENT_LOGOUT, (data) => {
            this.emit(AvaCAS.EVENT_LOGGED_OUT, { event: AvaCAS.EVENT_LOGGED_OUT, data });
        });
    }

    set token(token) {
        this[SYM_TOKEN] = token;
        this.emit(AvaCAS.EVENT_TOKEN, { event: AvaCAS.EVENT_TOKEN, data: { token } });
    }

    get token() {
        return this[SYM_TOKEN];
    }

    set userId(userId) {
        this[SYM_USER_ID] = userId;
        this.emit(AvaCAS.EVENT_USER_ID, { event: AvaCAS.EVENT_USER_ID, data: { userId } });
    }

    get userId() {
        return this[SYM_USER_ID];
    }

    strigifyState(state = {}) {
        return btoa(JSON.stringify(Object.assign(this.defaultState, state)));
    }

    goToLogin(state = {}) {
        // eslint-disable-next-line
        window.location.href = `${window.location.protocol}//${this.urls.login}?state=${this.strigifyState(state)}`;
    }

    goToPortal() {
        // eslint-disable-next-line
        window.location.href = this.defaultState.continue;
    }

    login({ username, password }) {
        return axios({
            url: `${this.settings.usersUrl}/v3/users/login`,
            method: 'POST',
            withCredentials: true,
            data: ({ username, password }),
        });
    }

    loginSocial({ type, accessToken }) {
        return axios({
            url: `${this.settings.usersUrl}/v3/users/login-social/${type}/${accessToken}?src=${this.iframeSrc}`,
            method: 'POST',
            withCredentials: true,
        });
    }

    register({ username, password, firstName, lastName }) {
        return axios({
            url: `${this.settings.usersUrl}/user`,
            method: 'POST',
            withCredentials: true,
            headers: { 'instance-url': this.iframeSrc },
            data: { username, password, firstName, lastName },
        });
    }

    resetPassword({ username }) {
        return axios({
            url: `${this.settings.usersUrl}/v3/users/forgot-password?src=${this.iframeSrc}`,
            method: 'POST',
            withCredentials: true,
            headers: { 'instance-url': this.iframeSrc },
            data: { username },
        });
    }

    recoverPassword({ username, token, password }) {
        return axios({
            url: `${this.settings.usersUrl}/v3/users/forgot-password/token`,
            method: 'POST',
            withCredentials: true,
            headers: { 'instance-url': this.iframeSrc },
            data: { username, token, password },
        });
    }

    verifyToken({ token }) {
        return axios({
            url: `${this.settings.usersUrl}/token/${token}`,
            method: 'POST',
            withCredentials: true,
            headers: { 'instance-url': this.iframeSrc },
        });
    }
    logout() {
        return axios({
            url: `${this.settings.usersUrl}/v3/users/logout`,
            method: 'POST',
            withCredentials: true,
            headers: { 'session-id': this.token },
        });
    }

    hasAccess(name, data) {
        return this
            .getToolbarData()
            .then((toolbarData) => {
                if (find(toolbarData.applications, { name })) {
                    return Promise.resolve(data);
                }
                return Promise.reject(`Access to ${name} denied`);
            });
    }

    getToolbarData({ applicationId = '' } = {}) {
        if (this.toolbarData) {
            return Promise.resolve(this.toolbarData);
        }
        const { token, iframeSrc, userId, settings, data: iframeData } = this;
        const wrapCall = (url, method = 'GET') => axios({
            method,
            url: `${this.settings.usersUrl}/${url}`,
            withCredentials: true,
            headers: { 'session-id': token, 'instance-url': iframeSrc },
        }).then(({ data }) => data);
        return Promise.all([
            wrapCall('current-user'),
            wrapCall(`user/${userId}/own-business`),
            wrapCall(`user/${userId}/associated-business`),
            wrapCall(`notification/receiver/${userId}`),
        ]).then(([user, ownBusiness, associatedBusiness, notifications] = []) => {
            const businesses = ownBusiness.concat(associatedBusiness);
            const currentUser = transformUser(user, businesses);
            return Promise.all([
                wrapCall(`business/${currentUser.guiSettings.defaultBusiness}`),
                wrapCall(`business/${currentUser.guiSettings.defaultBusiness}/application`),
            ]).then(([business, applications]) => {
                this.toolbarData = transformToolbarData({
                    usersUrl: settings.usersUrl,
                    urls: iframeData.urls,
                    currentUser,
                    notifications,
                    business,
                    businesses,
                    applications,
                    applicationId,
                });
                return this.toolbarData;
            });
        });
    }

    showLoader(elementId = 'ava-app-preloader') {
        this.preloaderContainer = document.getElementById(elementId);
        if (!this.preloaderContainer) {
            const copyrightContent = AvaCAS.getCopyrightContent();
            const template = `<style>
@keyframes blinker { 50% { opacity: 0; } }
#${elementId} { background: rgba(250, 250, 250, 1); color: rgba(0, 0, 0, 0.87); font-weight: 500; font-size: 14px; text-align: center; width: 100%; height: 100%; display: flex; flex-direction: column; align-items: center; justify-content: center; font-family: sans-serif; position: fixed; z-index: 9999; }
#${elementId} > img { width: 112px; height: 67px; }
#${elementId} > p { margin-top: 8px; margin-bottom: 0px; padding: 0px; }
#${elementId} > p.ava-app-blink { animation: blinker 1s linear infinite; }
#${elementId} > p.ava-app-copyright { color: rgba(0, 0, 0, 0.54); margin-top: 4px; margin-bottom: 0px; font-size: 10px; }
</style>
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHAAAABDCAYAAABEDoFIAAAABGdBTUEAALGPC/xhBQAACt1JREFUeAHtnAlsVWUWx3ldoa2ttSIDQqOiiNSMGcJUCDTKtLUUqyDRusQFl5nEMBIhUWaM48CQ6KioqHEGo8YYjXGajIAoXWmrLCqCITooQ4AYISCVZewyXaDt/M7Nu2++3rzlLu+9+97z3uTyred85zv/75zvfN+7xTfKe+Kmgba2try+vr4rfD5f8fDwcBFpFulAWlraTwhxOCcn59uysrLTVgTyWens9bWnAYDLALiZAHYVgKWH4zI0NLQfINvnzp3bF66f3uYBqGsiRummTZtyMjIyFsB+rDKEgPMDltdLmj04OHgB4OYp7T2U18+bN++UUhc06wEYVC3Rqdy1a1dmR0dHLWAU+Tl2nj17dntNTc1B6obUUTZv3jyR8mzqx0k9aW9eXt57c+bM6VL7GfMegEaNRLHc2Nj4G1zilX6Wh4uLiz8qKSkZCDUE7jWtvr7+WsDTaCgfra6u/ifl4VA0GaEavHp7GhCXmZ6efjnucQLgTfZzOTV27NgPAe9MOK4ANQRobYCYS/5i3gmtra2XQHMwFF1aqAav3poGxHoArzQzM/NeFF9GWQdvFPn2GTNmhAVPHw3a4fz8/HbSQakbGBi4Sm8LlnoABtOKxbq6urp03GU1wcpMwNKjzLOwOYUVHpo/f/4RKyxl3wPAQ0IDvwubm5sLQtF7LjSUZizUFxYWziI40SwOxf+Xd1tWVtYBjgICoq0Hfkdww5fBy0f+HtxqD4y+6erq2l1bWxvYRz0LtKXe/xNt3bq1EAVPlxqspTs7O/u9qqqqfU7AE15Yc7ekypNL/tcFBQW3c648V6/3ANQ1YTPFIn6pkxK8NAOcUfF6s6UUC/4RC2yVFzB3sjj+IwxwyQVcCizg2JEtZc+FihYcPHi4SUJOehrLO+yA1QhS/0L4l14JgDtxoxWMM5W6AlKx+k89C9Q1ZD/NF1JuU360zyIyJYANdXd3b8Ei5d5UnmnyjwegaMHBg2Voh2zcpwMu5kgJXlgngwf8vXOJfHNTxoXKhTFnpkr2iEp0WsokL+I9T1Yu5ePkj/FuZwU3T5o06ZNwNyL0M/3AX666zmPcC0wTOeiI/L3+NTOKfTIz6a/S5Cea/v7+ZShwCXrR7hEj6QelH+NdC806zmidkfqHa29oaJBD+6+kD8HGhsrKyu/D9Xfaxj5YBY/LhQ8B1LqkdqEor4KI7CBA/IX5mAJPJo7Cx0PzNNn9uKEaqbP7cGz4Gn6aG4VnxbZt286xyysSXUtLSxFDXSb9SI/KeTBpAWQlLkVhDUzEiesax56yCV4vwceWLogWJbz/UpSKPHmdnZ23EeJPg1/Utid4+ZDxIs6bN+E5NDnZc3fLmEnnQplMOpb3Kun9MgHDI1dQ7/I2076bC+QO3EwaIE04c+ZMCX1vpF5+mys00Mkx4C1+f7uPdMTPPMZ+wcrw9DU1Nc0DQM06pA98Bil3sWfZvo0RPvCAlU8O8aOlLA/lr5G1TctrNSH+kd+oEGAhTKZDdD7d+hH2EOi3FxUVtZi9oA3B3lY1K/FxZFitEiObXBQ/Tf2zkfY0uVdkJa+g/zL6B5Ti5/ci9A+rvM3m4SUgTmexlCJPplk6K/3gO4Tud1VUVHxOXnPbIS0QRd2EUPcxgMTH/QB5jHIOr+ayKH9H/WpWgkR3cXmwvFLG386ruqfTTKqSQ7TmUswKgrJnAOQG+l+o0sDrBnh9qNZZye/YsWMMVj8ZGSdCl8viV2W1wkrri55hNdwHn+NEnfsIkvRzoNYeFEA29oWspN+BsriA17G2wM8hKFECgLuhvob2E0ReDxmZWpbSBIGceRh3D5O5VOku8lWwiHYqdaaz/l/BP4MgACJz6oDnJYAol8cJ/4wAkAnlMwG5olnO24eylof6KYS+Yp03M9l2FPhMLGYqiwW+dyHHbN6ryQciTeQUd16FfB87GVsskcW6FV6qO30Evmuc8I0XrQagHII5Sy1mErLJayaPK3mZVVgfShD6STDxN9onjh49+h6isROh+lqt91v5GkCq1eUx8qDtKT43eMxYb6fMdvEk4/xRoT0+ZsyYiczJUQCi8ItZVhysr7e39zHc0yJG6UQx/8CqXmJVtoYblX6D9GsVesB/EKWXyUIIR2OmDVd5Kzy/pe8dpEH5MXYni+ZJM/zM9GEbkDPhaaXvOHQyWyknbDYDhZUjnXyzuA9A/mTR9x+WmQH+LJJZAPkdh81VRElydWX5YRH8Hl5yJhvh2oMwqvPf1gdpsl4lezhWuJFxFyvU88k7cs8Kr5hlMxC6WrgT4TxXXl5uaeMmuNlz8uTJJQCfR0RXg/LLOG89Ac+lYqFWpEaBi6B/2UgDn73we536Jtza99EEzjDWB5QXK3UPI5Pcq+5hfnUs7Garc1J4xSzrIxhZj2An2E9+63QUeD0Bj5nsn6uY8Odm+XE2K2YBfIWy1G8/5JPz5fD5O/JZPlybHVvvh+eYwmX4v/VykHQ/dQ8R3DQFaXOtKk2Uw2vrGskoNcA1Sh1ATDG2hStjtSuN4MGrmuj2lXiAJ7Ix3tFwMtImc2rEKldH6BfXZgFOLoPHixU4HRkQcoQHSg98dBOJp4xL/7vUfn7LCxtEqf2jkTfrmpnj4+zVf43GmNHgIVHoR8IIK3hErpnsMpUbCBZCrdCT7jHLB9e5EBkC0SZg7hW3aZY+mv1wjz79Zb89B6ucCf/XkEm7ttLHYn4rsMQb9bKbqRbtsaIeRahrEaST1d+OwD+g1BFCRxAyl+6y4Y+D9hP2U9MrlH1zM7y1QMo/xjKUuDbCeHFtJlKXT+TfYX7j9YGZ5wGOMle4fVbUVj4rfg1CHkHIW3htrSxZpbwNfP+/Tp+kmRSaKYa1klBBgswB/bSinzvRTQuyaoue9FKOTXJ2rjMzz1j10QBEiRLlvctB/H1c6VQELaQu0llMlamPY8h+VqOd25hfqIzkqKCWEyUvIOIs5DgTiNYBURa7+wDqSgIA+bs10/uXTucwteKqHQ7ljJw98Q1uqFQAS51xdE4dleODEzFYxSPCd66wHEfDTuQJR4uX2au246QCe6JaH8+86wAy2YOGCV9nKCdMkb88srKtxEVu1wEk6tWOMfpsWdUPYJWuy6XLo6Y9PT3T1LLRe6ht8cq7rigCpo1MNnBvilJKiPgejJcCrIzD/ne/2p/F9oVadiPvOoCc+Y6giDfVyQPq83L2UuvcznNwLxfvYJBDFp+rj+sAyuyJ7v5Mov4xfxarvZ4LhiWJ4E4FPOR7G1kCeyBgHuCrN/mmxtUnIJCrUjC4/2pqg6okkQlFxevnpBEqkC+++XWiRNymWF4QuRZx47R+BJELhYQBUOYOiEtR1AtkE8IzhMHjGVz/ijDtcWtKKABl1mKJgPgO2Zh9ou5Eu1jjs/zM9QdSub1y/Um4lY5b+oBvVKaiILm2CkSnbmsKeeQ/5xG3+WiigCc6STgLVIHi7lG+DF9AVHo99ZNR3ASsM0/tE6s8Y3Uz1lFSOSpslIDFjS/RYzU/j6+nAU8DngY8DXga8DTgacDTgKcBTwOeBjwNJKcGEvogb1elW7ZsCfqdDX/7kXLzTbirNLug/VzpPACTHHkPQA/AJNdAkovvWWCSA5gSURlR50pwkO9qrDyriEpXWiFIxL4pAaAo1iKIKQGezDtlALQAYsqAl3IAmgAxpcBLSQDDgJhy4KUsgEFATEnwZJ4p/Uhg4w9uUnae/wM3367fG02j0QAAAABJRU5ErkJggg==" />
    <p class="ava-app-blink">Loading content</p>
    <p class="ava-app-copyright">${copyrightContent}</p>`;
            this.preloaderContainer = document.createElement('div');
            this.preloaderContainer.setAttribute('id', elementId);
            this.preloaderContainer.innerHTML = template;
            document.body.appendChild(this.preloaderContainer);
        }
    }

    hideLoader(elementId = 'ava-app-preloader') {
        const elementToRemove = this.preloaderContainer || document.getElementById(elementId);
        if (elementToRemove) {
            elementToRemove.remove();
        }
        delete this.preloaderContainer;
    }

    renderAppSuspended(replaceElement, options) {
        return renderAppSuspended(this)(replaceElement, options);
    }

    static get applicationDefinitions() {
        return applicationDefinitions;
    }

    static getBrowserId() {
        const aKeys = ['MSIE', 'Firefox', 'Safari', 'Chrome', 'Opera', 'iPhone', 'iPad'];
        const sUsrAg = navigator.userAgent;
        let nIdx = aKeys.length - 1;

        for (nIdx; nIdx > -1 && sUsrAg.indexOf(aKeys[nIdx]) === -1; nIdx -= 1) {}

        return aKeys[nIdx];
    }

    injectCopyright() {
        document.getElementsByClassName('copyright')[0].innerHTML = AvaCAS.getCopyrightContent();
    }

    static getCopyrightContent() {
        return '© 2013-2019 Iota Networks, LLC.';
    }
}

export default AvaCAS;
