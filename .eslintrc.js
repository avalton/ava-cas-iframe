var path = require('path');

module.exports = {
    extends: 'airbnb',
    plugins: [
        'react',
        'import',
    ],
    parserOptions: {
        ecmaVersion: 6,
        sourceType: 'module',
    },
    rules: {
        indent: ['error', 4, { SwitchCase: 1 }],
        'no-param-reassign': ['error', { 'props': false }],
        'arrow-parens': 0,
    },
    env: {
        es6: true,
        browser: true,
    },
    globals: {
        _: false,
    },
};
